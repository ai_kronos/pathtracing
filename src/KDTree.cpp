#include"KDTree.h"
#include<algorithm>
#include<iostream>

KDTree::KDTree(Node* root) : root(root) {}

KDTree::~KDTree()
{
    delete root;
}

KDTree::Node::Node(
    Ogre::Real mx, Ogre::Real my, Ogre::Real mz,
    Ogre::Real Mx, Ogre::Real My, Ogre::Real Mz,
    Triangle* triangle
) : aabb(mx, my, mz, Mx, My, Mz), triangle(triangle)
{
    son[0] = son[1] = NULL;
}

KDTree::Node::~Node()
{
    delete son[0];
    delete son[1];
}

KDTree* KDTree::buildTree(const std::vector<Triangle*>& triangleList)
{
    std::vector<Triangle*> tmp(triangleList);
    return new KDTree(buildTree(0, tmp, 0, tmp.size() - 1));
}

KDTree::Node* KDTree::buildTree(int index, std::vector<Triangle*>& list, int l, int r)
{
    sortIndex = index;
    std::sort(list.begin() + l, list.begin() + r + 1, cmp);

    int mid = (l + r) / 2;
    Ogre::Real mx, my, mz, Mx, My, Mz;
    mx = list[l]->getMin(0);
    Mx = list[l]->getMax(0);
    my = list[l]->getMin(1);
    My = list[l]->getMax(1);
    mz = list[l]->getMin(2);
    Mz = list[l]->getMax(2);
    for(int i = l + 1; i <= r; ++i)
    {
        mx = std::min(mx, list[i]->getMin(0));
        Mx = std::max(Mx, list[i]->getMax(0));
        my = std::min(my, list[i]->getMin(1));
        My = std::max(My, list[i]->getMax(1));
        mz = std::min(mz, list[i]->getMin(2));
        Mz = std::max(Mz, list[i]->getMax(2));
    }
    Node* node = new Node(mx, my, mz, Mx, My, Mz, list[mid]);
    if(mid > l)
        node->son[0] = buildTree((index + 1) % 3, list, l, mid - 1);
    if(mid < r)
        node->son[1] = buildTree((index + 1) % 3, list, mid + 1, r);
    return node;
}

bool KDTree::cmp(Triangle* const l, Triangle* const r)
{
    return l->getMin(sortIndex) < r->getMin(sortIndex);
}

Triangle* KDTree::getIntersected(Ogre::Ray* ray)
{
    intersected = NULL;
    minT = 1e8;
    if(root != NULL)
        getIntersected(ray, root);
    return intersected;
}

void KDTree::getIntersected(Ogre::Ray* ray, Node* now)
{
    Ogre::RayTestResult test = ray->intersects(now->aabb); 
    if(!test.first || test.second > minT)
        return;
    
    Ogre::Vector3 result = now->triangle->getIntersection(ray);
    if(now->triangle->isIntersected(result) && result[2] < minT)
    {
        intersected = now->triangle;
        minT = result[2];
    }

    if(now->son[0] != NULL)
        getIntersected(ray, now->son[0]);
    if(now->son[1] != NULL)
        getIntersected(ray, now->son[1]);
}