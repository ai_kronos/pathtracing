#include"RayTracing.h"
#include"OgreStringConverter.h"
#include"BRDFRead.h"
#include<cmath>
#include<cstdlib>
#include<iostream>
#include<fstream>

RayTracing::RayTracing(Ogre::SceneManager* scnMgr, Ogre::Camera* camera, double P) : camera(camera), P(P)
{
    Triangle::retrieveTriangle(scnMgr, "Entity");
    Triangle::retrieveTriangle(scnMgr, "ManualObject");
    kdTree = KDTree::buildTree(Triangle::triangleList);
}

RayTracing::~RayTracing()
{
    for(auto i : brdfList)
        delete i.second;
    delete kdTree;
}

std::pair<double, double> RayTracing::getFresnel(
        double theta1, double n_1,
        double theta2, double n_2
)
{
    std::pair<double, double> result;
    double Rs = (n_1*cos(theta1) - n_2*cos(theta2)) /
                (n_1*cos(theta1) + n_2*cos(theta2));
    double Rp = (n_2*cos(theta1) - n_1*cos(theta2)) /
                (n_2*cos(theta1) + n_1*cos(theta2));
    result.first = (Rs * Rs + Rp * Rp) / 2;
    double Ts = (2.0*n_1*cos(theta1)) /
                (n_1*cos(theta1) + n_2*cos(theta2));
    double Tp = (2.0*n_1*cos(theta1)) /
                (n_2*cos(theta1) + n_1*cos(theta2));
    result.second = (Ts * Ts + Tp * Tp) / 2;
    return result;
}

Ogre::Vector3 RayTracing::getBRDF(
    Ogre::Pass* pass,
    double matKind,
    Ogre::Vector3 normal = Ogre::Vector3(0, 0, 0),
    Ogre::Vector3 in = Ogre::Vector3(0, 0, 0),
    Ogre::Vector3 out = Ogre::Vector3(0, 0, 0)
)
{
    double r, g, b;

    if(matKind < 1.5)//brdf
    {
        Ogre::ColourValue ambient = pass->getAmbient();
        Ogre::String brdfName = pass->getName();
        Ogre::Vector3 materialDir(ambient[0], ambient[1], ambient[2]);
        BRDFRead* brdf;

        if(brdfList.count(brdfName) == 0)
            brdfList[brdfName] = new BRDFRead(brdfName.c_str());

        brdf = brdfList[brdfName];

        Ogre::Vector3 matDir = normal.crossProduct(materialDir).crossProduct(normal);
        if(matDir.squaredLength() > 0.0001)
            matDir.normalise();
        else
            matDir = Ogre::Vector3(1, 0, 0);
        
        Ogre::Vector3 zaxis = matDir.crossProduct(normal);
/*
        std::pair<double, double> inAngle = convertCoord(normal, matDir, in);
        std::pair<double, double> outAngle = convertCoord(normal, matDir, out);
        brdf->lookup_brdf_val(inAngle.first, inAngle.second,
            outAngle.first, outAngle.second, r, g, b
        );
*/
        Ogre::Vector3 oin, oout;
        oin[0] = in.dotProduct(matDir);
        oout[0] = out.dotProduct(matDir);
        oin[2] = in.dotProduct(normal);
        oout[2] = out.dotProduct(normal);
        oin[1] = in.dotProduct(zaxis);
        oout[1] = out.dotProduct(zaxis);
        brdf->lookup_brdf_val(oin, oout, r, g, b);
    }
    else//ideally diffuse
    {
        Ogre::ColourValue diffuse = pass->getDiffuse();
        r = diffuse[0] / M_PI;
        g = diffuse[1] / M_PI;
        b = diffuse[2] / M_PI;
    }
    return Ogre::Vector3(r, g, b);
}

Ogre::Vector3 RayTracing::shade(Ogre::Ray ray, int type = 0)
{
    Triangle* triangle = kdTree->getIntersected(&ray);
    if(triangle == NULL)
        return Ogre::Vector3(0, 0, 0);
    Ogre::Vector3 posData = triangle->getIntersection(&ray);
    Ogre::Vector3 pos = ray.getPoint(posData[2]);
    Ogre::Pass* pass = triangle->getRenderablePtr()->getMaterial()->getTechnique(0)->getPass(0);
    Ogre::Vector3 normal = triangle->getNormal(Ogre::Vector3(posData[0], posData[1], 1.0 - posData[0] - posData[1]));

    Ogre::Ray newRay;
    Ogre::Vector3 result(0, 0, 0);
    Ogre::Vector3 accumulated(0, 0, 0);
    Ogre::ColourValue materialKind = pass->getSpecular();

    if(materialKind[0] > 0.5)//brdf
    {
    //Direct illumination
        int lightIndex = Triangle::lightList[rand() % Triangle::lightList.size()];
        Triangle* light = Triangle::triangleList[lightIndex];
        Ogre::Vector3 lightPosData = light->getRandPointData();
        Ogre::Vector3 lightPos = light->getPos(lightPosData);
        Ogre::Vector3 lightDir = lightPos - pos;
        double distance = lightDir.normalise() / 100;
        newRay.setOrigin(pos + lightDir);
        newRay.setDirection(lightDir);
        if(kdTree->getIntersected(&newRay) == light)
        {
            Ogre::Vector3 lightNormal = light->getNormal(lightPosData);
            Ogre::Pass* lightPass = light->getRenderablePtr()->getMaterial()->getTechnique(0)->getPass(0);
            Ogre::ColourValue _emissive = lightPass->getEmissive();
            Ogre::Vector3 emissive(_emissive[0], _emissive[1], _emissive[2]);
            distance = distance * distance;
            Ogre::Vector3 brdfFactor = getBRDF(
                pass, materialKind[0], normal, 
                -ray.getDirection(), lightDir
            );
            result = brdfFactor * (emissive / 4 / M_PI) * 
                normal.dotProduct(lightDir) * lightNormal.dotProduct(-lightDir) / distance *
                Triangle::lightList.size() * light->getArea();
            for(int i = 0; i < 3; ++i)
                if(result[i] < 0)
                    result[i] = -result[i];
        }

    //Indirect illumination
        if(getRand() < P)
        {
            Ogre::Vector3 randDir = getRandHalfSphere();
            Ogre::Vector3 direction = Ogre::Vector3::UNIT_Y.getRotationTo(normal) * randDir;
            newRay.setDirection(direction);
            newRay.setOrigin(pos + direction);
            Ogre::Vector3 brdfFactor = getBRDF(
                pass, materialKind[0], normal, 
                -ray.getDirection(), direction
            );

            accumulated = shade(newRay, type);
            accumulated *= brdfFactor * normal.dotProduct(direction);
            accumulated = accumulated * (2 * M_PI) / P;
        }
        return result + accumulated; 
    }
    else//fresnel
    {
        Ogre::ColourValue ambient = pass->getAmbient();
        Ogre::Vector3 reflect = 2 * normal.dotProduct(-ray.getDirection()) * normal + ray.getDirection();
        Ogre::Vector3 refract;
        Ogre::Vector3 result(0, 0, 0);
        for(int i = 1; i <= 3; ++i)
        {
            if(type == 0 || type == i)
            {
                double dirColor = 0, oColor = 0;
                double n1 = 1, n2 = ambient[i - 1];
                double cos1, cos2, theta1, theta2;
                double factor;

                cos1 = abs(normal.dotProduct(ray.getDirection()));
                theta1 = acos(cos1);


                if(normal.dotProduct(ray.getDirection()) > 0)
                    std::swap(n1, n2);
                if(!getRefract(n2 / n1, normal, ray.getDirection(), refract))
                {
                    cos2 = 0;
                    theta2 = M_PI / 2;
                    newRay.setOrigin(pos + reflect);
                    newRay.setDirection(reflect);
                    std::pair<double, double> fresnel = getFresnel(theta1, n1, theta2, n2);
                    factor = fresnel.first;
                }
                else
                {
                    cos2 = abs(normal.dotProduct(refract));
                    theta2 = acos(cos2);
                    std::pair<double, double> fresnel = getFresnel(theta1, n1, theta2, n2);
                    double p = fresnel.first;
                    if(getRand() < p)
                    {
                        newRay.setOrigin(pos + reflect);
                        newRay.setDirection(reflect);
                        factor = 1;
                    }
                    else
                    {
                        newRay.setOrigin(pos + refract);
                        newRay.setDirection(refract);
                        factor = 1;
                    }
                }

                Triangle* lightTriangle = kdTree->getIntersected(&newRay);
                if(lightTriangle != NULL)
                {
                    Ogre::Vector3 lightPosData = lightTriangle->getIntersection(&newRay);
                    Ogre::ColourValue _emissive = lightTriangle->getRenderablePtr()->getMaterial()->getTechnique(0)->getPass(0)->getEmissive();
                    Ogre::Vector3 emissive(_emissive[0], _emissive[1], _emissive[2]);
                    Ogre::Vector3 lightNormal = lightTriangle->getNormal(lightPosData);
                    if(emissive[0] + emissive[1] + emissive[2] > 1e-3 && lightNormal.dotProduct(newRay.getDirection()) < 0)
                    {
                        Ogre::Vector3 lightPosData = lightTriangle->getIntersection(&newRay);
                        double distance = lightPosData[2] / 100;
                        distance = distance * distance;
                        Ogre::Vector3 tmp = (emissive / 4 / M_PI) * 
                            abs(normal.dotProduct(newRay.getDirection())) * lightNormal.dotProduct(-newRay.getDirection()) / distance *
                            Triangle::lightList.size() * lightTriangle->getArea();
                        dirColor = tmp[i - 1];
                    }
                }
                if(getRand() < P)
                    oColor = shade(newRay, i)[i - 1];

                result[i - 1] = (oColor / P + dirColor) * factor;
            }
        }
        return result;
    }
}

bool RayTracing::getRefract(double n, const Ogre::Vector3 &normal, const Ogre::Vector3 &in, Ogre::Vector3 &refract)
{
    double cosTheta1 = in.dotProduct(normal);
    double sinTheta1 = sqrt(1.0 - cosTheta1 * cosTheta1);
    if(sinTheta1 >= n)
        return false;
    double sinTheta2 = sinTheta1 / n;
    double cosTheta2 = sqrt(1.0 - sinTheta2 * sinTheta2);
    Ogre::Vector3 _normal = normal;
    if(cosTheta1 < 0)
        _normal = -normal;
    Ogre::Vector3 tangent = (in - cosTheta1 * normal) / sinTheta1;
    refract = _normal * cosTheta2 + tangent * sinTheta2;
    return true;
}

double RayTracing::getRand()
{
    return double(rand() % 32767) / 32767;
}

Ogre::Vector3 RayTracing::getRandQuaSphere()
{
    Ogre::Vector3 result(getRand(), getRand(), getRand());
    while(result.squaredLength() > 1.0)
        result = Ogre::Vector3(getRand(), getRand(), getRand());
    result.normalise();
    return result;
}

Ogre::Vector3 RayTracing::getRandHalfSphere()
{
    int category = getRand() / 0.25;
    Ogre::Vector3 result = getRandQuaSphere();
    switch (category)
    {
    case 0:
        return result;
    case 1:
        return Ogre::Vector3(-result[0], result[1], result[2]);
    case 2:
        return Ogre::Vector3(-result[0], result[1], -result[2]);
    default:
        return Ogre::Vector3(result[0], result[1], -result[2]);
    }
}

std::pair<double, double> RayTracing::convertCoord(
    const Ogre::Vector3 &normal,
    const Ogre::Vector3 &matDir,
    const Ogre::Vector3 &lightDir
)
{
    double theta = acos(normal.dotProduct(lightDir));
    Ogre::Vector3 projLight = lightDir - theta * normal;
    if(projLight.squaredLength() > 0.0001)
        projLight.normalise();
    double dot = matDir.dotProduct(projLight);
    double cross = normal.dotProduct(projLight.crossProduct(matDir));
    double phi = acos(dot);
    if(cross < 0)
        phi = 2 * M_PI - phi;
    if(theta + theta > M_PI)
        theta = M_PI / 2;
    return std::make_pair(theta, phi);
}

void RayTracing::startRendering(int iterNum, const char* fileName)
{
    Ogre::Viewport* viewport = camera->getViewport();
    int pheight = viewport->getActualHeight(), pwidth = viewport->getActualWidth();
    int distance = camera->getNearClipDistance();
    double height = 2.0 * tan((camera->getFOVy().valueRadians() / 2)) * distance;
    double width = camera->getAspectRatio() * height;
    Ogre::Vector3 direction = camera->getRealDirection();
    Ogre::Vector3 viewPos = camera->getRealPosition();
    direction.normalise();
    Ogre::Vector3 dy = -Ogre::Vector3::UNIT_Y;
    Ogre::Vector3 dx = dy.crossProduct(direction);
    dx = dx * width / pwidth;
    dy = dy * height / pheight;
    Ogre::Vector3 origin = viewPos + direction * distance;
    origin = origin - dy * pheight / 2.0 - dx * pwidth / 2.0;
    Ogre::Vector3 *pixelBuffer = new Ogre::Vector3[pheight * pwidth];
    
    for(int y = 0, cnt = 0; y < pheight; ++y)
        for(int x = 0; x < pwidth; ++x, ++cnt)
            pixelBuffer[cnt] = Ogre::Vector3(0, 0, 0);

    Ogre::Ray newRay;
    newRay.setOrigin(viewPos);
    double area = 1;

    for(int i = 1; i <= iterNum; ++i)
    {
        int cnt = 0;
        for(int y = 0; y < pheight; ++y)
        {
            Ogre::Vector3 now = origin + y * dy;
            for(int x = 0; x < pwidth; ++x)
            {
                Ogre::Vector3 sample = now + getRand() * dx + getRand() * dy;
                Ogre::Vector3 dir = sample - viewPos;
                dir.normalise();
                double fac = 1.0 / i;
                newRay.setDirection(dir);
                Ogre::Vector3 color = shade(newRay);
                color = color * dir.dotProduct(direction) * area;

                Ogre::Vector3 result(0 ,0 ,0);
                Triangle* lightTriangle = kdTree->getIntersected(&newRay);
                if(lightTriangle != NULL)
                {
                    Ogre::Vector3 lightPosData = lightTriangle->getIntersection(&newRay);
                    Ogre::ColourValue _emissive = lightTriangle->getRenderablePtr()->getMaterial()->getTechnique(0)->getPass(0)->getEmissive();
                    Ogre::Vector3 emissive(_emissive[0], _emissive[1], _emissive[2]);
                    if(emissive[0] + emissive[1] + emissive[2] > 1e-3)
                    {
                        Ogre::Vector3 lightPosData = lightTriangle->getIntersection(&newRay);
                        Ogre::Vector3 lightNormal = lightTriangle->getNormal(lightPosData);
                        double distance = lightPosData[2] / 100;
                        distance = distance * distance;
                        result = (emissive / 4 / M_PI) * 
                            direction.dotProduct(newRay.getDirection()) * lightNormal.dotProduct(-newRay.getDirection()) / distance *
                            Triangle::lightList.size() * lightTriangle->getArea();
                        result = result * area;
                    }
                }

                pixelBuffer[cnt] = pixelBuffer[cnt] * (1 - fac) + fac * (result + color);
                now = now + dx;
                ++cnt;
            }
                std::cout << y << std::endl;
        }
        std::cout << "iter: " << i << std::endl;
        if(i % 10 == 1)
        {
            std::ofstream out(fileName, std::ios::out);
            out << "P3\n" << pwidth << " " << pheight << "\n255\n";
            int cnt = 0;
            for(int y = 0; y < pheight; ++y)
            {
                for(int x = 0; x < pwidth; ++x)
                {
                    int r = colorMapping(pixelBuffer[cnt][0]);
                    int g = colorMapping(pixelBuffer[cnt][1]);
                    int b = colorMapping(pixelBuffer[cnt][2]);
                    out << r << " " << g << " " << b << std::endl;
                    ++cnt;
                }
            }
            out.close();
        }
    }

    delete pixelBuffer;
}

int RayTracing::colorMapping(double color)
{
    return std::min(255.0, color * 255);
}