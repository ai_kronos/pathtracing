struct VpInput
{
    float4 pos : POSITION;
    float3 normal : NORMAL0;
};

struct FpInput
{
    float4 pos : POSITION;
    float3 normal : TEXCOORD0;
};

FpInput genVP(VpInput vpInput, 
            uniform float4x4 transMatrix,
            uniform float3x3 normalMatrix)
{
    FpInput fpInput;
    fpInput.pos = mul(transMatrix, vpInput.pos);
    fpInput.normal = normalize(mul(normalMatrix, vpInput.normal));

    return fpInput;
}

float4 genFP(FpInput fpInput) :COLOR0
{
    return float4(((fpInput.normal + 1) / 2), 1);
}