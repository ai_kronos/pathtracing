#include<iostream>
#include"OgreApplicationContext.h"
#include"RayTracing.h"


class TestTriangle : public OgreBites::ApplicationContext
{
private:
    Ogre::Root* root;
    Ogre::SceneManager* scnMgr;

    Ogre::RTShader::ShaderGenerator* shadergen;
    Ogre::RenderWindow* renderWid;
    Ogre::SceneNode* charNode;

public:
    TestTriangle();
    void setup();
    void test();
};

TestTriangle::TestTriangle() : OgreBites::ApplicationContext("Ogre_TestTriangle") {}

void TestTriangle::setup()
{
    OgreBites::ApplicationContext::setup();
    root = OgreBites::ApplicationContext::getRoot();
    scnMgr = root->createSceneManager();

    shadergen = Ogre::RTShader::ShaderGenerator::getSingletonPtr();
    shadergen->addSceneManager(scnMgr);

    Ogre::RenderWindow* renderWid = getRenderWindow();
//    scnMgr->setAmbientLight(Ogre::ColourValue(0.2, 0.2, 0.2));
//    scnMgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);

    Ogre::Camera* camera = scnMgr->createCamera("camera");
    renderWid->addViewport(camera);
    camera->setNearClipDistance(5);
    Ogre::SceneNode* camNode = scnMgr->getRootSceneNode()->createChildSceneNode();
    camNode->attachObject(camera);
    camNode->setPosition(0, 0, 99);
    camNode->lookAt(Ogre::Vector3(0, 0, 0), Ogre::Node::TS_PARENT);
    camera->setFOVy(Ogre::Radian(3.14/1.75));
/*
    Ogre::ManualObject* bezier = scnMgr->createManualObject("Bezier");
    bezier->begin("Bezier", Ogre::RenderOperation::OT_TRIANGLE_STRIP);
    bezier->position(3, 0, 0);
    bezier->position(0, 3, 0);
    bezier->position(0, 0, 3);
    bezier->end();
    Ogre::SceneNode* bNode = scnMgr->getRootSceneNode()->createChildSceneNode();
    bNode->attachObject(bezier);
*/
    Ogre::Light* light = scnMgr->createLight("sunlight");
    light->setType(Ogre::Light::LT_POINT);
    Ogre::SceneNode* ligNode = scnMgr->getRootSceneNode()->createChildSceneNode();
    ligNode->attachObject(light);
    ligNode->setDirection(-1, -1, 0);
    ligNode->setPosition(90, 90, 0);

    Ogre::Entity* slight = scnMgr->createEntity("sphere.mesh");
    Ogre::SceneNode* sligNode = scnMgr->getRootSceneNode()->createChildSceneNode();
    sligNode->attachObject(slight);
    sligNode->setPosition(-40, -60, 0);
    sligNode->scale(0.4, 0.4, 0.4);

    Ogre::Entity* slight2 = scnMgr->createEntity("sphere.mesh");
    Ogre::SceneNode* sligNode2 = scnMgr->getRootSceneNode()->createChildSceneNode();
    sligNode2->attachObject(slight2);
    sligNode2->setPosition(30, -60, -40);
    sligNode2->scale(0.4, 0.4, 0.4);

    Ogre::MeshManager::getSingleton().createPlane(
        "ground",
        Ogre::RGN_DEFAULT,
        Ogre::Plane(Ogre::Vector3::UNIT_Y, -100),
        200, 200, 1, 1,
        true,
        0, 1, 1,
        Ogre::Vector3::UNIT_Z
    );

    scnMgr->getRootSceneNode()->createChildSceneNode()->attachObject(scnMgr->createEntity("bottom", "ground"));
    scnMgr->getRootSceneNode()->createChildSceneNode()->attachObject(scnMgr->createEntity("left", "ground"));
    scnMgr->getRootSceneNode()->createChildSceneNode()->attachObject(scnMgr->createEntity("right", "ground"));
    scnMgr->getRootSceneNode()->createChildSceneNode()->attachObject(scnMgr->createEntity("top", "ground"));
    scnMgr->getRootSceneNode()->createChildSceneNode()->attachObject(scnMgr->createEntity("back", "ground"));
    scnMgr->getRootSceneNode()->createChildSceneNode()->attachObject(scnMgr->createEntity("front", "ground"));
    scnMgr->getEntity("left")->getParentSceneNode()->roll(-Ogre::Radian(3.14/2));
    scnMgr->getEntity("right")->getParentSceneNode()->roll(Ogre::Radian(3.14/2));
    scnMgr->getEntity("top")->getParentSceneNode()->roll(Ogre::Radian(3.14));
    scnMgr->getEntity("back")->getParentSceneNode()->pitch(-Ogre::Radian(3.14/2));
    scnMgr->getEntity("front")->getParentSceneNode()->pitch(Ogre::Radian(3.14/2));

    Ogre::MeshManager::getSingleton().createPlane(
        "lig",
        Ogre::RGN_DEFAULT,
        Ogre::Plane(-Ogre::Vector3::UNIT_Y, -99),
        20, 20, 1, 1,
        true,
        0, 1, 1,
        Ogre::Vector3::UNIT_Z
    );
    scnMgr->getRootSceneNode()->createChildSceneNode()->attachObject(scnMgr->createEntity("lig", "lig"));

/*
    Ogre::Entity* entity = scnMgr->createEntity("sphere.mesh");
    Ogre::SceneNode* sphereNode = scnMgr->getRootSceneNode()->createChildSceneNode();
    sphereNode->attachObject(entity);
    sphereNode->scale(0.1, 0.1, 0.1);
    sphereNode->setPosition(0, -80, -100);
    */

    scnMgr->getEntity("front")->setMaterialName("white");
    slight2->setMaterialName("glass");
    slight->setMaterialName("metallic");
//    entity->setMaterialName("white");
    scnMgr->getEntity("lig")->setMaterialName("whiteLight");
    scnMgr->getEntity("bottom")->setMaterialName("white");
    scnMgr->getEntity("left")->setMaterialName("red");
    scnMgr->getEntity("right")->setMaterialName("blue");
    scnMgr->getEntity("top")->setMaterialName("white");
    scnMgr->getEntity("back")->setMaterialName("white");

    RayTracing rayTracing(scnMgr, camera, 0.7);
    rayTracing.startRendering(1000, "test.ppm");
}

void TestTriangle::test()
{
}

int main()
{
    TestTriangle testTriangle;
    testTriangle.initApp();
//    testTriangle.test();
//    testTriangle.getRoot()->startRendering();
    testTriangle.closeApp();
    return 0;
}