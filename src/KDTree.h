#ifndef KDTREE_H
#define KDTREE_H

#include"Triangle.h"

class KDTree
{
    class Node;

public:
    static KDTree* buildTree(const std::vector<Triangle*> &);

private:
    static Node* buildTree(int, std::vector<Triangle*>&, int, int);
    static bool cmp(Triangle* const, Triangle* const);
    inline static int sortIndex;
    inline static Triangle* intersected;
    inline static Ogre::Real minT;

private:
    class Node
    {
    public:
        Node* son[2];
        Ogre::AxisAlignedBox aabb;
        Triangle* triangle;

    public:
        Node() = delete;
        Node(
            Ogre::Real,
            Ogre::Real,
            Ogre::Real,
            Ogre::Real,
            Ogre::Real,
            Ogre::Real,
            Triangle*
        );
        ~Node();
    }*root = NULL;

public:
    KDTree() = delete;
    ~KDTree();
    Triangle* getIntersected(Ogre::Ray*);

private:
    KDTree(Node*);
    void getIntersected(Ogre::Ray*, Node*);
};

#endif