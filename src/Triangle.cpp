#include "Triangle.h"
#include "RayTracing.h"
#include <map>
#include <algorithm>
#include <string>
#include <iostream>

void Triangle::retrieveTriangle(Ogre::SceneManager* scnMgr, const std::string component)
{
    int start = triangleList.size();
    if(component == "Entity")
        retrieveTriangle_E(scnMgr);
    if(component == "ManualObject")
        retrieveTriangle_M(scnMgr);
    for(int i = start; i < triangleList.size(); ++i)
    {
        Triangle* triangle = triangleList[i];
        Ogre::Pass* pass = triangle->getRenderablePtr()->getMaterial()->getTechnique(0)->getPass(0);
        Ogre::ColourValue _emissive = pass->getEmissive();
        Ogre::Vector3 emissive(_emissive[0], _emissive[1], _emissive[2]);
        if(emissive[0] + emissive[1] + emissive[2] > 1e-3)
            lightList.push_back(i);
    }
}

void Triangle::retrieveTriangle_M(Ogre::SceneManager* scnMgr)
{
    Ogre::SceneManager::MovableObjectIterator mIt
        = scnMgr->getMovableObjectIterator("ManualObject");
    std::map<void*, int> baseIndex;

    while(mIt.hasMoreElements())
    {
        Ogre::ManualObject* manualObject;
        manualObject = static_cast<Ogre::ManualObject*>(mIt.getNext());

        int numSubEntity = manualObject->getNumSections();

        for(int i = 0; i < numSubEntity; ++i)
        {
            Ogre::Renderable* renderable = manualObject->getSection(i);
            Ogre::SubMesh* subMesh = new Ogre::SubMesh; 
            static_cast<Ogre::ManualObject::ManualObjectSection*>(renderable)->convertToSubMesh(subMesh);
            
            int offset = vertexCount;
            int num = subMesh->vertexData->vertexCount;
            if(!subMesh->useSharedVertices)
            {
                baseIndex[static_cast<void*>(subMesh)] = vertexCount;
                retrieveVertex(subMesh->vertexData, vertexCount);
            }

            switch (subMesh->operationType)
            {
            case Ogre::RenderOperation::OT_TRIANGLE_LIST:
                {
                    for(int j = 0; j < num; )
                    {
                        int v0, v1, v2;
                        v0 = offset + j++;
                        v1 = offset + j++;
                        v2 = offset + j++;
                        triangleList.push_back(new Triangle(v0, v1, v2, renderable));
                    }
                    break;
                }
            case Ogre::RenderOperation::OT_TRIANGLE_FAN:
                {
                    int v0, v1, v2, j = 0;
                    v0 = offset + j++;
                    v1 = offset + j++;
                    for(; j < num; )
                    {
                        v2 = offset + j++;
                        triangleList.push_back(new Triangle(v0, v1, v2, renderable));
                        v1 = v2;
                    }
                    break;
                }
            case Ogre::RenderOperation::OT_TRIANGLE_STRIP:
                {
                    int v0, v1, v2, j = 0;
                    v0 = offset + j++;
                    v1 = offset + j++;
                    for(; j < num; )
                    {
                        v2 = offset + j++;
                        triangleList.push_back(new Triangle(v0, v1, v2, renderable));
                        v0 = v1;
                        v1 = v2;
                    }
                    break;
                }
            }
        }
    }
}

void Triangle::retrieveTriangle_E(Ogre::SceneManager* scnMgr)
{
    Ogre::SceneManager::MovableObjectIterator mIt
        = scnMgr->getMovableObjectIterator("Entity");
    std::map<void*, int> baseIndex;

    while(mIt.hasMoreElements())
    {
        int sharedIndex = vertexCount;
        Ogre::Entity* entity = static_cast<Ogre::Entity*>(mIt.getNext());
        Ogre::MeshPtr mesh = entity->getMesh();

        if(baseIndex.count(static_cast<void*>(mesh.getPointer())) == 0)
        {
            baseIndex[static_cast<void*>(mesh.getPointer())] = vertexCount;
            retrieveVertex(mesh->sharedVertexData, vertexCount);
        }
        else
        {
            sharedIndex = baseIndex[static_cast<void*>(mesh.getPointer())];
        }
        int numSubEntity = entity->getNumSubEntities();

        for(int i = 0; i < numSubEntity; ++i)
        {
            Ogre::Renderable* renderable = entity->getSubEntity(i);
            Ogre::SubMesh* subMesh = static_cast<Ogre::SubEntity*>(renderable)->getSubMesh();
            
            if(!subMesh->useSharedVertices && baseIndex.count(static_cast<void*>(subMesh)) == 0)
            {
                baseIndex[static_cast<void*>(subMesh)] = vertexCount;
                retrieveVertex(subMesh->vertexData, vertexCount);
            }
            int offset;
            

            Ogre::IndexData* indexData = subMesh->indexData;
            Ogre::HardwareIndexBufferSharedPtr iBuf = indexData->indexBuffer;
            unsigned short* pShort;
            unsigned int* pInt;
            bool use32Bit = (iBuf->getType() == Ogre::HardwareIndexBuffer::IT_32BIT);
            if(use32Bit)
                pInt = static_cast<unsigned int*>(iBuf->lock(Ogre::HardwareBuffer::HBL_READ_ONLY));
            else
                pShort = static_cast<unsigned short*>(iBuf->lock(Ogre::HardwareBuffer::HBL_READ_ONLY));

            if(subMesh->useSharedVertices)
                offset = sharedIndex;
            else
                offset = baseIndex[static_cast<void*>(subMesh)];
            

            switch (subMesh->operationType)
            {
            case Ogre::RenderOperation::OT_TRIANGLE_LIST:
                {
                    int size = indexData->indexCount / 3;
                    for(int j = 0; j < size; ++j)
                    {
                        int v0, v1, v2;
                        if(use32Bit)
                        {
                            v0 = offset + *pInt++;
                            v1 = offset + *pInt++;
                            v2 = offset + *pInt++;
                        }
                        else
                        {
                            v0 = offset + *pShort++;
                            v1 = offset + *pShort++;
                            v2 = offset + *pShort++;
                        }
                        triangleList.push_back(new Triangle(v0, v1, v2, renderable));
                    }
                    break;
                }
            case Ogre::RenderOperation::OT_TRIANGLE_FAN:
                {
                    int size = indexData->indexCount - 2;
                    int v0, v1, v2;
                    if(use32Bit)
                    {
                        v0 = offset + *pInt++;
                        v1 = offset + *pInt++;
                    }
                    else
                    {
                        v0 = offset + *pShort++;
                        v1 = offset + *pShort++;
                    }
                    for(int j = 0; j < size; ++j)
                    {
                        if(use32Bit)
                            v2 = offset + *pInt++;
                        else
                            v2 = offset + *pShort++;
                        triangleList.push_back(new Triangle(v0, v1, v2, renderable));
                        v1 = v2;
                    }
                    break;
                }
            case Ogre::RenderOperation::OT_TRIANGLE_STRIP:
                {
                    int size = indexData->indexCount - 2;
                    int v0, v1, v2;
                    if(use32Bit)
                    {
                        v0 = offset + *pInt++;
                        v1 = offset + *pInt++;
                    }
                    else
                    {
                        v0 = offset + *pShort++;
                        v1 = offset + *pShort++;
                    }
                    for(int j = 0; j < size; ++j)
                    {
                        if(use32Bit)
                            v2 = offset + *pInt++;
                        else
                            v2 = offset + *pShort++;
                        triangleList.push_back(new Triangle(v0, v1, v2, renderable));
                        v0 = v1;
                        v1 = v2;
                    }
                    break;
                }
            }
            iBuf->unlock();
        }
    }
}

void Triangle::retrieveVertex(Ogre::VertexData* vertexData, int& vertexCount)
{
    if(vertexData == NULL)
        return;
    Ogre::HardwareVertexBufferSharedPtr vBuf;
    char* baseAdd;
    int startIndex = vertexCount;

    const Ogre::VertexElement* posEle
        = vertexData->vertexDeclaration->findElementBySemantic(Ogre::VES_POSITION);
    const Ogre::VertexElement* normalEle
        = vertexData->vertexDeclaration->findElementBySemantic(Ogre::VES_NORMAL);
    Ogre::Real* pReal;

    if (posEle != NULL)
    {
        vBuf = vertexData->vertexBufferBinding->getBuffer(posEle->getSource());
        baseAdd = static_cast<char*>(vBuf->lock(Ogre::HardwareBuffer::HBL_READ_ONLY));
        for(int i = 0; i < vertexData->vertexCount; ++i, baseAdd += vBuf->getVertexSize())
        {
            posEle->baseVertexPointerToElement(baseAdd, &pReal);
            vertexList.push_back(new Vertex);
            vertexList[vertexCount++]->pos = Ogre::Vector3(pReal[0], pReal[1], pReal[2]);
        }
        vBuf->unlock();
    }

    if (normalEle != NULL)
    {
        vBuf = vertexData->vertexBufferBinding->getBuffer(normalEle->getSource());
        baseAdd = static_cast<char*>(vBuf->lock(Ogre::HardwareBuffer::HBL_READ_ONLY));
        for(int i = 0; i < vertexData->vertexCount; ++i, baseAdd += vBuf->getVertexSize())
        {
            normalEle->baseVertexPointerToElement(baseAdd, &pReal);
            vertexList[i + startIndex]->normal = Ogre::Vector3(pReal[0], pReal[1], pReal[2]);
        }
        vBuf->unlock();
    }
}

Triangle::Triangle(int a, int b, int c, Ogre::Renderable* renderable)
{
    index[0] = a;
    index[1] = b;
    index[2] = c;
    renderablePtr = renderable;
}

Ogre::Vector3 Triangle::getPos(int index)
{
    Ogre::Vector3 pos;
    switch (index)
    {
    case 0:
        pos = vertexList[this->index[0]]->pos;
        break;
    case 1:
        pos = vertexList[this->index[1]]->pos;
        break;
    case 2:
        pos = vertexList[this->index[2]]->pos;
        break;
    }
    Ogre::Matrix4 worldMatrix;
    renderablePtr->getWorldTransforms(&worldMatrix);
    return (worldMatrix * Ogre::Vector4(pos, 1)).xyz();
}

Ogre::Vector3 Triangle::getPos(Ogre::Vector3 factor)
{
    Ogre::Matrix3 mat;
    mat.SetColumn(0, vertexList[index[0]]->pos);
    mat.SetColumn(1, vertexList[index[1]]->pos);
    mat.SetColumn(2, vertexList[index[2]]->pos);
    mat = mat.transpose();
    Ogre::Vector4 pos = Ogre::Vector4(factor * mat, 1);
    Ogre::Matrix4 worldMatrix;
    renderablePtr->getWorldTransforms(&worldMatrix);
    return (worldMatrix * pos).xyz();
}

Ogre::Vector3 Triangle::getNormal(Ogre::Vector3 factor)
{
    Ogre::Matrix3 mat;
    mat.SetColumn(0, vertexList[index[0]]->normal);
    mat.SetColumn(1, vertexList[index[1]]->normal);
    mat.SetColumn(2, vertexList[index[2]]->normal);
    mat = mat.transpose();
    Ogre::Vector3 normal = factor * mat;
    Ogre::Matrix3 worldMatrix;
    Ogre::Matrix4 _worldMatrix;
    renderablePtr->getWorldTransforms(&_worldMatrix);
    _worldMatrix.extract3x3Matrix(worldMatrix);
    worldMatrix = worldMatrix.inverse().transpose();
    normal = worldMatrix * normal;
    normal.normalise();
    return normal;
}

Ogre::Vector3 Triangle::getIntersection(Ogre::Ray* ray)
{
    Ogre::Vector3 v0 = getPos(0);
    Ogre::Vector3 v1 = getPos(1);
    Ogre::Vector3 v2 = getPos(2);
    Ogre::Matrix3 left;
    left.SetColumn(0, v0 - v2);
    left.SetColumn(1, v1 - v2);
    left.SetColumn(2, -ray->getDirection());
    Ogre::Vector3 right = ray->getOrigin() - v2;
    left = left.inverse();
    return right * left.transpose();
}

Ogre::Real Triangle::getMax(int index)
{
    Ogre::Real ans = getPos(0)[index];
    ans = std::max(ans, getPos(1)[index]);
    return std::max(ans, getPos(2)[index]);
}

Ogre::Real Triangle::getMin(int index)
{
    Ogre::Real ans = getPos(0)[index];
    ans = std::min(ans, getPos(1)[index]);
    return std::min(ans, getPos(2)[index]);
}

bool Triangle::isIntersected(Ogre::Vector3 factor)
{
    return  factor[0] > -eps &&
            factor[0] < eps + 1 &&
            factor[1] > -eps &&
            factor[1] < eps + 1 &&
            factor[0] + factor[1] < eps + 1 &&
            factor[2] > eps;
}

bool Triangle::isIntersected(Ogre::Ray* ray)
{
    return isIntersected(getIntersection(ray));
}

Ogre::Renderable* Triangle::getRenderablePtr()
{
    return renderablePtr;
}

Ogre::Vector3 Triangle::getRandPointData()
{
    double a = RayTracing::getRand();
    double b = (1.0 - a) * RayTracing::getRand();
    return Ogre::Vector3(a, b, 1.0 - a - b);
}

double Triangle::getArea()
{
    Ogre::Vector3 v0 = getPos(1) - getPos(0);
    Ogre::Vector3 v1 = getPos(2) - getPos(0);
    double length = v0.crossProduct(v1).length();
    return length / 2 / 10000;
}