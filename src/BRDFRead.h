#ifndef BRDFREAD_H
#define BRDFREAD_H

#include <vector>
#include"Ogre.h"

class BRDFRead
{
private:
    double* brdf;

public:
    BRDFRead() = delete;
    BRDFRead(const char*);
    void lookup_brdf_val(double, double, double, double, double&, double&, double&);
    void lookup_brdf_val(const Ogre::Vector3&, const Ogre::Vector3&, double&, double&, double&);
    ~BRDFRead();

private:
    bool read_brdf(const char *);
    static int phi_diff_index(double phi_diff);
    static int theta_diff_index(double theta_diff);
    static int theta_half_index(double theta_half);
    static void std_coords_to_half_diff_coords(double, double, double, double, double&, double&, double&, double&);
    static void std_coords_to_half_diff_coords(const Ogre::Vector3&, const Ogre::Vector3&, double&, double&, double&, double&);
    static void rotate_vector(double* vector, double* axis, double angle, double* out);
    static void normalize(double* v);
    static void cross_product (double* v1, double* v2, double* out);
};

#endif