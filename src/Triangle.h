#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "Ogre.h"
#include <vector>
#include <string>

class Vertex
{
public:
    Ogre::Vector3 pos, normal;
    //Ogre::Vector2 texcoord;
};

class Triangle
{
public:
    inline static std::vector<Vertex*> vertexList;
    inline static std::vector<Triangle*> triangleList;
    inline static std::vector<int> lightList;

private:
    inline static int vertexCount = 0;
    inline static const Ogre::Real eps = 1e-6;
    int index[3];
    Ogre::Renderable* renderablePtr;

public:
    Triangle(int, int, int, Ogre::Renderable*);
    static void retrieveTriangle(Ogre::SceneManager*, const std::string);

    Ogre::Vector3 getPos(int);
    Ogre::Vector3 getPos(Ogre::Vector3);
    Ogre::Vector3 getNormal(Ogre::Vector3);
    Ogre::Vector3 getIntersection(Ogre::Ray*);
    Ogre::Vector3 getRandPointData();
    double getArea();
    Ogre::Real getMax(int);
    Ogre::Real getMin(int);
    bool isIntersected(Ogre::Ray*);
    bool isIntersected(Ogre::Vector3);
    Ogre::Renderable* getRenderablePtr();

private:
    static void retrieveVertex(Ogre::VertexData*, int&);
    static void retrieveTriangle_E(Ogre::SceneManager*);
    static void retrieveTriangle_M(Ogre::SceneManager*);
};

#endif