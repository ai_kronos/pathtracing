#ifndef RAY_TRACING
#define RAY_TRACING

#include"KDTree.h"
#include"BRDFRead.h"
#include<vector>
#include<map>
#include<string>

class RayTracing
{
private:
    KDTree* kdTree;
    Ogre::Camera* camera;
    std::map<std::string, BRDFRead*> brdfList;
    const double P;

public:
    RayTracing(Ogre::SceneManager*, Ogre::Camera*, double);
    void startRendering(int, const char*);
    ~RayTracing();
    static double getRand();
    static std::pair<double, double> getFresnel(double, double, double, double);

private:
    inline static const double M_PI = 3.1415926535897932384626433832795;

    Ogre::Vector3 shade(Ogre::Ray, int);
    Ogre::Vector3 getBRDF(Ogre::Pass*, double, Ogre::Vector3, Ogre::Vector3, Ogre::Vector3);
    static Ogre::Vector3 getRandQuaSphere();
    static Ogre::Vector3 getRandHalfSphere();
    static std::pair<double, double> convertCoord(const Ogre::Vector3&, const Ogre::Vector3&, const Ogre::Vector3&);
    static bool getRefract(double, const Ogre::Vector3 &, const Ogre::Vector3&, Ogre::Vector3&);
    static int colorMapping(double);
};

#endif